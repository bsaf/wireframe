# Wireframe

Static site framework.

## Prerequisites

- Sass: `gem install sass`
- CoffeeScript: `sudo npm install -g coffee-script`

## Usage

1. Clone the repo
2. Rename the project
3. Delete the .git folder and create a new one for your project
4. Run `rake s` to start the server and watchers
